package com.company;

import java.util.*;

public class Main {

    static ArrayList<Integer> playerPositions = new ArrayList<>();
    static ArrayList<Integer> cpuPositions = new ArrayList<>();

    public static void main(String[] args) {

        char [] [] gameboard = {{' ', '|', ' ', '|', ' '},
                                {'-', '+', '-', '+', '-'},
                                {' ', '|', ' ', '|', ' '},
                                {'-', '+', '-', '+', '-'},
                                {' ', '|', ' ', '|', ' '},};

        printGameboard(gameboard);

        while(true) {

            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter you placement (1-9) :");
            int playerPlacement = scanner.nextInt();

            while (playerPositions.contains(playerPlacement) || cpuPositions.contains(playerPlacement)) {
                System.out.println("Position taken! Enter a correct position");
                playerPlacement = scanner.nextInt();
            }
            piecePlacement(gameboard, playerPlacement, "Player");
            String result = checkWinner();
            if(result.length() > 0) {
                printGameboard(gameboard);
                System.out.println(result);
                break;
            }

            Random rand = new Random();
            int cpuPlacement = rand.nextInt(9) + 1;
            while (playerPositions.contains(cpuPlacement) || cpuPositions.contains(cpuPlacement)) {
//                System.out.println("Position taken! Enter a correct position");
                cpuPlacement = rand.nextInt(9) + 1;
            }
            piecePlacement(gameboard,cpuPlacement,"CPU");
            result = checkWinner();
            if(result.length() > 0) {
                printGameboard(gameboard);
                System.out.println(result);
                break;
            }

            printGameboard(gameboard);
        }
    }



    public static void printGameboard (char [] [] gameboard) {
        for(char [] row : gameboard){
            for (char c : row){
                System.out.print(c);
            }
            System.out.println();
        }
    }

    public static void piecePlacement (char [][] gameboard, int position, String user) {
        
        char symbolToPlace = 'A';
        
        if (user.equals("Player")) {
            symbolToPlace = 'X';
            playerPositions.add(position);
        }
        else if(user.equals("CPU")) {
            symbolToPlace = 'O';
            cpuPositions.add(position);
        }
        switch (position) {
            case 1:
                if(gameboard [0] [0] == ' ') {
                    gameboard [0] [0] = symbolToPlace;
                }
                break;
            case 2:
                if(gameboard [0] [2] == ' ') {
                    gameboard [0] [2] = symbolToPlace;
                }
                break;
            case 3:
                if(gameboard [0] [4] == ' ') {
                    gameboard [0] [4] = symbolToPlace;
                }
                break;
            case 4:
                if(gameboard [2] [0] == ' ') {
                    gameboard [2] [0] = symbolToPlace;
                }
                break;
            case 5:
                if(gameboard [2] [2] == ' ') {
                    gameboard [2] [2] = symbolToPlace;
                }
                break;
            case 6:
                if(gameboard [2] [4] == ' ') {
                    gameboard [2] [4] = symbolToPlace;
                }
                break;
            case 7:
                if(gameboard [4] [0] == ' ') {
                    gameboard [4] [0] = symbolToPlace;
                }
                break;
            case 8:
                if(gameboard [4] [2] == ' ') {
                    gameboard [4] [2] = symbolToPlace;
                }
                break;
            case 9:
                if(gameboard [4] [4] == ' ') {
                    gameboard [4] [4] = symbolToPlace;
                }
                break;
        }
    }

    public static String checkWinner() {

        String winnerPlayer = "Congratulations, you won!!!";
        String winnerCPU = "Sorry, you lost :(";
        String tie = "TIE!";
        String result = "";

        List topRow = Arrays.asList(1, 2 ,3);
        List midRow = Arrays.asList(4, 5 ,6);
        List bottomRow = Arrays.asList(7, 8 ,9);
        List firstCollum = Arrays.asList(1, 4 ,7);
        List secondCollum = Arrays.asList(2, 5 ,8);
        List thirdCollum = Arrays.asList(3, 6 ,9);
        List leftDiagonal = Arrays.asList(1, 5 ,9);
        List rightDiagonal = Arrays.asList(7, 5 ,3);

        List<List> winningConditions =  new ArrayList<>();
        winningConditions.add(topRow);
        winningConditions.add(midRow);
        winningConditions.add(bottomRow);
        winningConditions.add(firstCollum);
        winningConditions.add(secondCollum);
        winningConditions.add(thirdCollum);
        winningConditions.add(leftDiagonal);
        winningConditions.add(rightDiagonal);

        for (List l : winningConditions) {
            if (playerPositions.containsAll(l)) {
                result = winnerPlayer;
            }
            else if (cpuPositions.containsAll(l)) {
                result = winnerCPU;
            }
            else if (playerPositions.size() + cpuPositions.size() == 9) {
                result = tie;
            }
        }
        return result;
    }
}

